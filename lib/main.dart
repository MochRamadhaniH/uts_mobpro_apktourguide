import 'package:flutter/material.dart';
import 'package:tugas2_mobpro_kelompokmelinda/TravelApp.dart';
import 'package:tugas2_mobpro_kelompokmelinda/informasitempat.dart';
import 'login_page.dart';
import 'home_page.dart';
import 'welcome.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    Welcome.tag: (context) => Welcome(),
    HomePage.tag: (context) => HomePage(),
    Travelapp.tag: (context) => Travelapp(),
    Info.tag: (context) => Info(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aplikasi Login',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
        fontFamily: 'Lato',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
