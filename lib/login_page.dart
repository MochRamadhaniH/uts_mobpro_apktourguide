import 'package:flutter/material.dart';
import 'package:tugas2_mobpro_kelompokmelinda/welcome.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'LogoSTTB',
      child: CircleAvatar(
        backgroundColor: Colors.black,
        radius: 48.0,
        child: Image.asset('assets/images/logoapk.png'),
      ),
    );
    final sapaan = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang di Aplikasi Tour Guide',
        style: TextStyle(fontSize: 28.0, color: Colors.black),
      ),
    );

    final username = TextFormField(
      keyboardType: TextInputType.name,
      autofocus: false,
      initialValue: 'Melinda',
      decoration: InputDecoration(
        hintText: 'Nama Pengguna',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Welcome.tag);
          },
          color: Colors.lightBlueAccent,
          child: Text('Login', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            sapaan,
            SizedBox(height: 48.0),
            username,
            loginButton,
          ],
        ),
      ),
    );
  }
}
