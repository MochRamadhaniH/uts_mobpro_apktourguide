import 'package:flutter/material.dart';

class Info extends StatefulWidget {
  static String tag = 'info-page';
  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Info Tempat',
      home: HomeScreen(),
      routes: <String, WidgetBuilder>{
        '/screen': (BuildContext context) => new Screen(),
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget phoneSection = Container(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(bottom: 32.0),
                  child: Text(
                    'Welcome To Indonesia',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );

    Widget buttonSection = RaisedButton(
      child: const Text('Lets Have a Tour'),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      color: Theme.of(context).accentColor,
      elevation: 4.0,
      splashColor: Colors.blueGrey,
      onPressed: () {
        Navigator.of(context).pushNamed('/screen');
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Tour Guide Melinda'),
      ),
      body: ListView(
        children: <Widget>[
          Image.asset(
            'images/bgindo.jpg',
            width: 600.0,
            height: 240.0,
            fit: BoxFit.cover,
          ),
          phoneSection,
          buttonSection,
        ],
      ),
    );
  }

  void button1(BuildContext context) {
    Navigator.of(context).pushNamed('/screen');
  }
}

class Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            bottom: new TabBar(
              isScrollable: true,
              tabs: <Widget>[
                new Tab(text: 'Tempat', icon: Icon(Icons.local_activity)),
                new Tab(text: 'Wisata', icon: Icon(Icons.local_parking)),
                new Tab(text: 'Makanan', icon: Icon(Icons.local_dining)),
              ],
            ),
          ),
          body: new Container(
            padding: new EdgeInsets.all(16.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: new TabBarView(
                    children: <Widget>[
                      new ListView(
                        children: list1,
                        scrollDirection: Axis.vertical,
                      ),
                      new ListView(
                        children: list2,
                        scrollDirection: Axis.vertical,
                      ),
                      new ListView(
                        children: list3,
                        scrollDirection: Axis.vertical,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

List<Widget> list1 = <Widget>[
  new Container(
    padding: const EdgeInsets.all(8.0),
    child: new Image.asset(
      'images/Jakarta.jpg',
      height: 100.0,
      width: 50.0,
      fit: BoxFit.cover,
    ),
  ),
  new ListTile(
    title: new Text('Jakarta',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Daerah Khusus Ibukota Jakarta (DKI Jakarta) adalah ibu kota negara dan kota terbesar di Indonesia. Jakarta merupakan satu-satunya kota di Indonesia yang memiliki status setingkat provinsi. Jakarta terletak di pesisir bagian barat laut Pulau Jawa.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new Container(
    padding: const EdgeInsets.all(8.0),
    child: new Image.asset(
      'images/Bandung.jpg',
      height: 100.0,
      width: 50.0,
      fit: BoxFit.cover,
    ),
  ),
  new ListTile(
    title: new Text('Bandung',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Kota Bandung adalah kota metropolitan terbesar di Provinsi Jawa Barat, sekaligus menjadi ibu kota provinsi tersebut. Kota ini terletak 140 km sebelah tenggara Jakarta, dan merupakan kota terbesar di wilayah Pulau Jawa bagian selatan.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new Container(
    padding: const EdgeInsets.all(8.0),
    child: new Image.asset(
      'images/Jogja.jpg',
      height: 100.0,
      width: 50.0,
      fit: BoxFit.cover,
    ),
  ),
  new ListTile(
    title: new Text('Daerah Istimewa Yogyakarta',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Daerah Istimewa setingkat provinsi di Indonesia yang merupakan peleburan Negara Kesultanan Yogyakarta dan Negara Kadipaten Paku Alaman. Daerah Istimewa Yogyakarta terletak di bagian selatan Pulau Jawa, dan berbatasan dengan Provinsi Jawa Tengah dan Samudera Hindia.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new Container(
    padding: const EdgeInsets.all(8.0),
    child: new Image.asset(
      'images/Bali.jpg',
      height: 100.0,
      width: 50.0,
      fit: BoxFit.cover,
    ),
  ),
  new ListTile(
    title: new Text('Bali',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
];
List<Widget> list2 = <Widget>[
  new ListTile(
    title: new Text('Taman Mini Indonesia Indah',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'aman Mini Indonesia Indah (TMII) merupakan suatu kawasan taman wisata bertema budaya Indonesia di Jakarta Timur. Area seluas kurang lebih 150 hektare[1] atau 1,5 kilometer persegi ini terletak pada koordinat 6°18′6.8″LS,106°53′47.2″BT. Taman ini merupakan rangkuman kebudayaan bangsa Indonesia, yang mencakup berbagai aspek kehidupan sehari-hari masyarakat 26 provinsi Indonesia (pada tahun 1975) yang ditampilkan dalam anjungan daerah berarsitektur tradisional, serta menampilkan aneka busana, tarian, dan tradisi daerah. Di samping itu, di tengah-tengah TMII terdapat sebuah danau yang menggambarkan miniatur kepulauan Indonesia di tengahnya, kereta gantung, berbagai museum, dan Teater IMAX Keong Mas dan Teater Tanah Airku), '),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new ListTile(
    title: new Text('Dago Dreampark Bandung',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Selain menyajikan pemandangan alam dengan hutan pinusnya yang indah, nyaman dan asri, Dago Dreampark juga mempunyai wahana-wahana bermain, spot foto serta menampilkan bangunan-bangunan bernuansa Jawa dan Bali.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new ListTile(
    title: new Text('Jalan Malioboro',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Jalan Malioboro adalah jalan perbelanjaan utama di Yogyakarta, Indonesia; nama ini juga digunakan secara lebih umum untuk lingkungan sekitar jalan. Itu terletak sumbu utara-selatan di garis antara Kraton Yogyakarta dan Gunung Merapi. Ini sendiri penting bagi banyak penduduk lokal, orientasi utara-selatan antara istana dan gunung berapi menjadi penting.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
];
List<Widget> list3 = <Widget>[
  new ListTile(
    title: new Text('Kerak Telor',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Kerak telor adalah hidangan omelet pedas tradisional Betawi dalam masakan Indonesia. Terbuat dari ketan yang dimasak dengan telur dan disajikan dengan serundeng, bawang merah goreng dan udang kering sebagai toppingnya.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new ListTile(
    title: new Text('Surabi',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Serabi, juga disebut surabi, srabi, juga dikenal di Thailand sebagai khanom khrok, adalah pancake Indonesia yang terbuat dari tepung beras dengan santan atau kelapa parut sebagai pengemulsi.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new ListTile(
    title: new Text('Wedang Jahe',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'wedang jahe merupakan minuman herbal yang terbuat dari akar jahe. '),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
  new ListTile(
    title: new Text('Sate Lilit',
        style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
    subtitle: new Text(
        'Sate lilit adalah varian sate di Indonesia yang berasal dari masakan khas Bali. Sate ini terbuat dari daging babi cincang, ikan, ayam, daging sapi, atau bahkan daging penyu, yang kemudian dicampur dengan kelapa parut, santan kental, air jeruk nipis, bawang merah, dan merica.'),
  ),
  new Divider(
    height: 8.0,
    color: Colors.blue,
  ),
];
