import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tugas2_mobpro_kelompokmelinda/TravelApp.dart';
import 'package:tugas2_mobpro_kelompokmelinda/informasitempat.dart';

class HomePage extends StatelessWidget {
  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    final foto = Hero(
      tag: 'Toga',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/images/logoapk.png'),
        ),
      ),
    );
    final selamatDatang = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Halo Melinda',
        style: TextStyle(fontSize: 28.0, color: Colors.black),
      ),
    );
    final isi = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang di Aplikasi Travel',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final form = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Aplikasi ini berfungsi untuk menayangkan beberapa informasi dari tempat pariwisata ingin anda tujui',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final nextButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.redAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Travelapp.tag);
          },
          color: Colors.lightBlueAccent,
          child: Text('TEMPAT WISATA', style: TextStyle(color: Colors.white)),
        ),
      ),
    );
    final infoButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.redAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(Info.tag);
          },
          color: Colors.lightBlueAccent,
          child:
              Text('INFORMASI TEMPAT', style: TextStyle(color: Colors.white)),
        ),
      ),
    );
    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[
          foto,
          selamatDatang,
          isi,
          form,
          nextButton,
          infoButton
        ],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
