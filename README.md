# tugas2_mobpro_kelompokmelinda

Tugas Mobile Programming 2 Kelompok :
- ALfazri Fuad Fharudin 18111009,
- Melinda Fitri Y 18111091,
- Moch Ramadhani H 18111095,
- Sugiman 17111267,
- Wildan Ma'ruf 18111170.

# Getting Started

Projek ini kami buat menggunakan tools :

- Visual Studio Code
- Flutter and Dart

# About project

Kami mengerjakan sebuah projek yang bertemakan tentang tour guide yang bertujuan untuk memberikan informasi dari tempat pariwisata yang ada di indonesia.

Projek tersusun dari 5 page yang terdiri dari :

- Login page 
- Welcome page
- Home page
- Places page
- Information page

Job Desc :
- Moch. Ramadhani H Coder
- Alfazri Fuad Fharudin Support Tech
- Melinda Fitri Y Captain
- Wildan Ma'ruf ide
- sugiman support link

# Screenshots project
SS projek kami di android
Login Page
![SS1](/uploads/d17056a959ecb5ec611a2bf746b891ce/SS1.jpg)
Welcome Page
![SS2](/uploads/f464df31bd9ee57650a15cf60fa4053b/SS2.jpg)
Home Page
![SS3](/uploads/116724301212a68dbc60553a83027364/SS3.jpg)
Places Page
![SS4](/uploads/3f100a56739fb15df33941a904bc6175/SS4.jpg)
Information Page
![SS5](/uploads/4f141d0acf7870d7838046881ebf2732/SS5.jpg)

